from unittest.mock import MagicMock, PropertyMock

import pytest


@pytest.fixture(scope="function")
def mocked_update() -> MagicMock:
    update = MagicMock()

    user = MagicMock()
    user.id = 1337
    user.username = "test"
    user.first_name = "test"
    user.last_name = "test"
    user.phone = "+79532022680"

    message = MagicMock()
    message.reply_text = MagicMock()
    message.text = "+79532022680"

    update.message = message
    update.message.from_user = user

    return update


@pytest.fixture(scope="function")
def mocked_context() -> MagicMock:
    context = MagicMock()
    return context
