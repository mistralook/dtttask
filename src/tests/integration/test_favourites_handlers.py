import pytest

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.users.db.models import User

from app.internal.utils.constants import ZERO_FAVOURITES, TEMPLATE_ADD_TO_FAVOURITES, USER_NOT_IN_FAVOURITE_LIST
from app.internal.bot_handlers.handlers import start_command, show_favourites, add_to_favourites, \
    remove_from_favourites, phone_num_handler


@pytest.mark.django_db
@pytest.mark.integration
def test_show_zero_favourites(mocked_update: Update):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)

    show_favourites(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(ZERO_FAVOURITES)


@pytest.mark.django_db
@pytest.mark.integration
def test_add_without_args_favourites(mocked_update: Update, mocked_context: CallbackContext):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)
    mocked_context.args = None
    add_to_favourites(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(TEMPLATE_ADD_TO_FAVOURITES)


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favourites(mocked_update: Update, mocked_context: CallbackContext, test_user: User):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)
    mocked_context.args = [test_user.username]
    add_to_favourites(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(
        f"{test_user.username} was successfully added to your favourites list"
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_show_favourites(mocked_update: Update, mocked_context: CallbackContext, test_user: User):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)

    mocked_context.args = [test_user.username]
    add_to_favourites(mocked_update, mocked_context)

    show_favourites(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(
        f"Your favourites:\n"
        f"{test_user.first_name} (@{test_user.username})"
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_remove_favourite(mocked_update: Update, mocked_context: CallbackContext, test_user: User):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)

    mocked_context.args = [test_user.username]
    add_to_favourites(mocked_update, mocked_context)

    remove_from_favourites(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(
        f"{test_user.username} was successfully removed from your favourites list"
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_remove_when_not_in_favourite(mocked_update: Update, mocked_context: CallbackContext, test_user: User):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)

    mocked_context.args = [test_user.username]
    add_to_favourites(mocked_update, mocked_context)

    remove_from_favourites(mocked_update, mocked_context)
    remove_from_favourites(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(USER_NOT_IN_FAVOURITE_LIST)
