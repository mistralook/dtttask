import pytest

from telegram import Update
from app.internal.utils.constants import HELP_MESSAGE, USER_ALREADY_CREATED, NUMBER_WAS_ADDED, \
    COMMAND_BEFORE_START, NO_PHONE
from app.internal.bot_handlers.handlers import help_command, start_command, me, phone_num_handler


@pytest.mark.django_db
@pytest.mark.integration
def test_help_command(mocked_update: Update):
    help_command(mocked_update, None)
    mocked_update.message.reply_text.assert_called_once_with(HELP_MESSAGE)


@pytest.mark.django_db
@pytest.mark.integration
def test_start_command_when_no_user(mocked_update: Update):
    start_command(mocked_update, None)
    user_first_name = mocked_update.message.from_user.first_name
    mocked_update.message.reply_text.assert_called_once_with(f"Пользователь был сохранен, {user_first_name}!")


@pytest.mark.django_db
@pytest.mark.integration
def test_start_command_when_user_created(mocked_update: Update):
    start_command(mocked_update, None)
    start_command(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(USER_ALREADY_CREATED)


@pytest.mark.django_db
@pytest.mark.integration
def test_me_before_start(mocked_update: Update):
    me(mocked_update, None)
    mocked_update.message.reply_text.assert_called_once_with(COMMAND_BEFORE_START)


@pytest.mark.django_db
@pytest.mark.integration
def test_me_after_start(mocked_update: Update):
    start_command(mocked_update, None)
    me(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(NO_PHONE)


@pytest.mark.django_db
@pytest.mark.integration
def test_start_and_phone(mocked_update: Update):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(NUMBER_WAS_ADDED)


@pytest.mark.django_db
@pytest.mark.integration
def test_me_after_start_phone(mocked_update: Update):
    start_command(mocked_update, None)
    phone_num_handler(mocked_update, None)
    me(mocked_update, None)

    user_info = mocked_update.message.from_user
    message = f"Информация о текущем пользователе\n" \
              f"Имя: {user_info.first_name} {user_info.last_name}\n" \
              f"Имя пользователя: {user_info.username}\n" \
              f"Номер телефона: {user_info.phone}"

    mocked_update.message.reply_text.assert_called_with(message)
