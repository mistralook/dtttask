import pytest

from telegram import Update
from telegram.ext import CallbackContext
from app.internal.utils.constants import TEMPLATE_CARD_BALANCE, CARD_NOT_FOUND, \
    RIGHT_CARD_FORMATTING, TEMPLATE_ACCOUNT_BALANCE, NO_PARTNERS
from app.internal.bot_handlers.bank_handlers import balance_by_card, balance_by_account, bank_operations_partners, \
    bank_operations

from app.internal.bank_operations.db.models import BankOperation


@pytest.mark.django_db
@pytest.mark.integration
def test_balance_card_no_args(mocked_update: Update, mocked_context: CallbackContext):
    mocked_context.args = None
    balance_by_card(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(TEMPLATE_CARD_BALANCE)


@pytest.mark.django_db
@pytest.mark.integration
def test_balance_by_card(mocked_update: Update, mocked_context: CallbackContext, test_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account

    mocked_update.message.from_user = user
    mocked_context.args = [str(card.card_number)]

    balance_by_card(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with("1000.00")


@pytest.mark.django_db
@pytest.mark.integration
def test_balance_by_wrong_format_card(mocked_update: Update, mocked_context: CallbackContext,
                                      test_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account

    mocked_update.message.from_user = user
    mocked_context.args = ["1234"]

    balance_by_card(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(RIGHT_CARD_FORMATTING)


@pytest.mark.django_db
@pytest.mark.integration
def test_balance_by_unknown_card(mocked_update: Update, mocked_context: CallbackContext,
                                 test_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account

    mocked_update.message.from_user = user
    mocked_context.args = ["2" * 16]

    balance_by_card(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(CARD_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_balance_by_account(mocked_update: Update, mocked_context: CallbackContext, test_user_with_card_and_account):
    user, account, card = test_user_with_card_and_account

    mocked_update.message.from_user = user
    mocked_context.args = [str(account.account_number)]

    balance_by_account(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with("1000.00")


@pytest.mark.django_db
@pytest.mark.integration
def test_operation_partners_no_args(mocked_update: Update,
                                    test_user_with_card_and_account,
                                    test_another_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    bank_operations_partners(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(NO_PARTNERS)


@pytest.mark.django_db
@pytest.mark.integration
def test_operation_partners(mocked_update: Update,
                            test_user_with_card_and_account,
                            test_another_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    another_user, _, another_card = test_user_with_card_and_account

    mocked_update.message.from_user = user
    BankOperation.objects.create(source_card=card, receiver_card=another_card, amount=150)
    BankOperation.objects.create(source_card=card, receiver_card=another_card, amount=1)
    BankOperation.objects.create(source_card=card, receiver_card=another_card, amount=2)

    bank_operations_partners(mocked_update, None)
    mocked_update.message.reply_text.assert_called_with(f"@{another_user.username}")


@pytest.mark.django_db
@pytest.mark.integration
def test_bank_operations_wrong_args(mocked_update: Update, mocked_context: CallbackContext):
    mocked_context.args = ["1234"]
    bank_operations(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_with(RIGHT_CARD_FORMATTING)


@pytest.mark.django_db
@pytest.mark.integration
def test_bank_operations(mocked_update: Update,
                         mocked_context: CallbackContext,
                         test_user_with_card_and_account,
                         test_another_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    another_user, _, another_card = test_another_user_with_card_and_account

    mocked_update.message.from_user = user
    mocked_context.args = [str(card.card_number)]

    BankOperation.objects.create(source_card=card, receiver_card=another_card, amount=228)
    BankOperation.objects.create(source_card=another_card, receiver_card=card, amount=1337)

    bank_operations(mocked_update, mocked_context)
    message = f'History of transactions: \n' \
              f'From: 1111111111111111(@test) \n' \
              f'To: 9999999999999999(@test_another) \n' \
              f'Amount: 228.00 \n' \
              f'From: 9999999999999999(@test_another) \n' \
              f'To: 1111111111111111(@test) \n' \
              f'Amount: 1337.00 \n'
    mocked_update.message.reply_text.assert_called_with(message)
