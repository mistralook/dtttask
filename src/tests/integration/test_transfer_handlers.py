import pytest

from telegram import Update
from telegram.ext import CallbackContext
from app.internal.utils.constants import TEMPLATE_TRANSFER, SELF_TRANSFER_ERROR, \
    INVALID_TRANSFER_ARGUMENTS, NOT_ENOUGH_MONEY, SOURCE_CARD_NOT_EXIST, RECEIVER_CARD_NOT_EXIST, \
    RECEIVER_USER_NOT_EXIST, RECEIVER_USER_ZERO_CARDS
from app.internal.bot_handlers.bank_handlers import transfer_money


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_no_args(mocked_update: Update, mocked_context: CallbackContext):
    mocked_context.args = None
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(TEMPLATE_TRANSFER)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_invalid_amount_of_args(mocked_update: Update, mocked_context: CallbackContext):
    mocked_context.args = ["9999999999999999"]
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(INVALID_TRANSFER_ARGUMENTS)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_to_yourself(mocked_update: Update, mocked_context: CallbackContext):
    mocked_context.args = ["1414131314141313",
                           "1414131314141313",
                           "1"]
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(SELF_TRANSFER_ERROR)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_invalid_amount(mocked_update: Update, mocked_context: CallbackContext):
    mocked_context.args = ["1414131314141313",
                           "9898989898989898",
                           "aboba"]
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(
        "Invalid amount. Reason: amount must be a positive number."
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_source_card_not_exist(mocked_update: Update, mocked_context: CallbackContext):
    source_card = "5" * 16
    destination_card = "0" * 16
    mocked_context.args = [source_card, destination_card, "1337"]
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(SOURCE_CARD_NOT_EXIST)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_destination_card_not_exist(mocked_update: Update, mocked_context: CallbackContext,
                                                  test_user_with_card_and_account):
    _, _, card = test_user_with_card_and_account
    destination_card = "0" * 16
    mocked_context.args = [str(card.card_number), destination_card, "1337"]
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(RECEIVER_CARD_NOT_EXIST)



@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_not_enough_money(mocked_update: Update, mocked_context: CallbackContext,
                                        test_user_with_card_and_account, test_another_user_with_card_and_account):
    _, _, source_card = test_user_with_card_and_account
    _, _, destination_card = test_another_user_with_card_and_account
    mocked_context.args = [str(source_card.card_number), str(destination_card.card_number), "1337"]
    transfer_money(mocked_update, mocked_context)
    mocked_update.message.reply_text.assert_called_once_with(NOT_ENOUGH_MONEY)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_when_everything_okay(mocked_update: Update, mocked_context: CallbackContext,
                                       test_user_with_card_and_account, test_another_user_with_card_and_account):
    user, _, source_card = test_user_with_card_and_account
    another_user, _, destination_card = test_another_user_with_card_and_account
    amount = 555
    mocked_context.args = [str(source_card.card_number), str(destination_card.card_number), str(amount)]
    transfer_money(mocked_update, mocked_context)
    message = f"The operation was successful. \n" \
              f"From: {source_card.card_number} \n" \
              f"To: {destination_card.card_number} \n" \
              f"{amount} were sent"
    mocked_update.message.reply_text.assert_called_once_with(message)

