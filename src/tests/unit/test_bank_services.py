import pytest
import decimal

from app.internal.bank_operations.db.models import BankOperation
# from app.internal.services.bank_services import get_card_by_card_number, get_balance_by_card, operations_partners, \
#     get_all_operations
from app.internal.bank_operations.db.repositories import BankOperationRepository
from app.internal.cards.db.repositories import CardRepository
from app.internal.utils.constants import NO_PARTNERS, CARD_NOT_FOUND, NO_OPERATIONS

bank_operation_repo = BankOperationRepository()
card_repo = CardRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_by_card_number(test_user_with_card_and_account):
    _, _, card = test_user_with_card_and_account
    assert card_repo.get_card_by_card_number(card_number=card.card_number) == card


@pytest.mark.unit
@pytest.mark.django_db
def test_get_balance_by_card(test_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    _, balance = card_repo.get_balance(t_id=user.id, card_number=card.card_number)
    assert balance == card.balance


@pytest.mark.unit
@pytest.mark.django_db
def test_zero_operations_partners(test_user_with_card_and_account, test_another_user_with_card_and_account,
                                  test_third_user_with_card_and_account):
    user, _, _ = test_user_with_card_and_account
    assert bank_operation_repo.operations_partners(t_id=user.id) is False


@pytest.mark.unit
@pytest.mark.django_db
def test_get_operations_partners(test_user_with_card_and_account, test_another_user_with_card_and_account,
                                 test_third_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    another_user, _, another_card = test_another_user_with_card_and_account
    third_user, _, third_card = test_third_user_with_card_and_account
    amount = decimal.Decimal(150)
    BankOperation.objects.create(source_card=card, receiver_card=another_card, amount=amount)
    BankOperation.objects.create(source_card=third_card, receiver_card=card, amount=amount)
    BankOperation.objects.create(source_card=another_card, receiver_card=card, amount=amount)
    usernames = bank_operation_repo.operations_partners(t_id=user.id)
    usernames = f", ".join([f'@{u[0]}' for u in usernames])
    assert usernames == f"@{third_user.username}, @{another_user.username}"


@pytest.mark.unit
@pytest.mark.django_db
def test_get_all_operations_no_card():
    assert bank_operation_repo.get_all_operations(123, 1414141414141414) is False


@pytest.mark.unit
@pytest.mark.django_db
def test_no_operations_with_card(test_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    assert bank_operation_repo.get_all_operations(user.id, card.card_number) is False


@pytest.mark.unit
@pytest.mark.django_db
def test_get_not_your_operations(test_user_with_card_and_account, test_another_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    another_user, _, another_card = test_another_user_with_card_and_account
    assert bank_operation_repo.get_all_operations(user.id, another_card.card_number) == False


@pytest.mark.unit
@pytest.mark.django_db
def test_get_all_operations(test_user_with_card_and_account, test_another_user_with_card_and_account,
                            test_third_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    another_user, _, another_card = test_another_user_with_card_and_account
    third_user, _, third_card = test_third_user_with_card_and_account
    amount = decimal.Decimal(150)
    BankOperation.objects.create(source_card=card, receiver_card=another_card, amount=amount)
    BankOperation.objects.create(source_card=third_card, receiver_card=card, amount=amount)
    operations = bank_operation_repo.get_all_operations(user.id, card.card_number)
    formatted_operations = [
        f'From: {operation["source_card"]}(@{operation["source_card__account__account_holder__username"]}) \n' \
        f'To: {operation["receiver_card"]}(@{operation["receiver_card__account__account_holder__username"]}) \n' \
        f'Amount: {operation["amount"]} \n'
        for operation in operations]
    formatted_operations = "History of transactions: \n" + "".join(formatted_operations)
    assert formatted_operations == f'History of transactions: \n' \
                                   f'From: 1111111111111111(@test) \n' \
                                   f'To: 9999999999999999(@test_another) \n' \
                                   f'Amount: 150.00 \n' \
                                   f'From: 8888888888888888(@lololoshkin) \n' \
                                   f'To: 1111111111111111(@test) \n' \
                                   f'Amount: 150.00 \n'
