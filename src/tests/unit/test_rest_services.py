import datetime
import pytest

# from app.internal.services.rest_service import revoke_tokens, revoke_token, validate_token, validate_password, \
#     get_token
from app.internal.authentication.db.repositories import IssuedTokenRepository

issued_token_repo = IssuedTokenRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_get_bad_token(test_tokens):
    assert issued_token_repo.get_token(refresh_token="refresh") is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_token(test_tokens):
    refresh, _ = test_tokens
    assert issued_token_repo.get_token(refresh_token=refresh).jti == refresh


@pytest.mark.unit
@pytest.mark.django_db
def test_validate_token_if_revoked(test_tokens):
    refresh, _ = test_tokens
    refresh = issued_token_repo.get_token(refresh_token=refresh)
    refresh.revoked = True
    refresh.save()
    flag, validation_message = issued_token_repo.validate_token(refresh)
    assert validation_message == "refresh token was revoked"


@pytest.mark.unit
@pytest.mark.django_db
def test_validate_token_if_expired(test_tokens):
    refresh, _ = test_tokens
    refresh = issued_token_repo.get_token(refresh_token=refresh)
    refresh.created_at = datetime.datetime.today() - datetime.timedelta(days=5)
    refresh.save()
    _, validation_message = issued_token_repo.validate_token(refresh)
    assert validation_message == "refresh token was expired"


@pytest.mark.unit
@pytest.mark.django_db
def test_validate_good_token(test_tokens):
    refresh, _ = test_tokens
    refresh = issued_token_repo.get_token(refresh_token=refresh)
    is_valid, validation_message = issued_token_repo.validate_token(refresh)
    assert is_valid is True and validation_message == ""
