import pytest

from app.internal.users.db.models import User
from app.internal.users.db.repositories import UserRepository

user_repo = UserRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_add_to_user_favourites(test_user: User, test_user_to_add: User):
    user_repo.add_to_user_favourites(test_user.id, test_user_to_add.username)
    assert test_user.favourites.all()[0].username == test_user_to_add.username


@pytest.mark.unit
@pytest.mark.django_db
def test_show_user_favourites(test_user: User, test_user_to_add: User):
    user_repo.add_to_user_favourites(test_user.id, test_user_to_add.username)
    favourites = user_repo.user_favourites(test_user.id)
    assert favourites[0].username == test_user_to_add.username


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_existing_user_from_favourites(test_user: User, test_user_to_add: User):
    user_repo.add_to_user_favourites(test_user.id, test_user_to_add.username)
    status = user_repo.remove_from_user_favourites(test_user.id, test_user_to_add.username)
    assert status is True
