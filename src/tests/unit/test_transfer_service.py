import decimal

import pytest

from app.internal.bank_operations.db.repositories import BankOperationRepository
from app.internal.cards.db.repositories import CardRepository

bank_operation_repo = BankOperationRepository()
card_repo = CardRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_good_transfer(test_user_with_card_and_account, test_another_user_with_card_and_account):
    user, _, card = test_user_with_card_and_account
    another_user, _, another_card = test_another_user_with_card_and_account
    initial_card_balance = card.balance
    initial_another_card_balance = another_card.balance
    transfer_amount = decimal.Decimal(228)
    card_repo.transfer_money(card.card_number, another_card.card_number, transfer_amount)
    _, card_balance = card_repo.get_balance(user.id, card.card_number)
    _, another_card_balance = card_repo.get_balance(another_user.id, another_card.card_number)
    assert card_balance == initial_card_balance - transfer_amount
    assert another_card_balance == initial_another_card_balance + transfer_amount


@pytest.mark.unit
@pytest.mark.django_db
def test_bad_transfer_not_enough_money(test_user_with_card_and_account, test_another_user_with_card_and_account):
    _, _, card = test_user_with_card_and_account
    _, _, another_card = test_another_user_with_card_and_account
    transfer_amount = decimal.Decimal(1500)
    status = card_repo.transfer_money(card.card_number, another_card.card_number, transfer_amount)
    assert status is False
