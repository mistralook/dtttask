import pytest

from app.internal.users.db.models import User


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_user_to_add(first_name="user_to_add", username="user_to_add_username", id=2, phone="+79530022648"):
    return User.objects.create(first_name=first_name,
                               last_name="",
                               username=username,
                               id=id,
                               phone=phone)
