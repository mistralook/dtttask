import pytest

from app.internal.users.db.models import User
from app.internal.users.db.repositories import UserRepository
from app.internal.utils.utils import hash_password

from app.internal.utils.exceptions import InvalidPasswordException

user_repo = UserRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_get_user_via_id(test_user: User):
    assert user_repo.get_user(test_user.id) == test_user


@pytest.mark.unit
@pytest.mark.django_db
def test_get_user_via_username(test_user: User):
    assert user_repo.get_user_via_username(test_user.username) == test_user


@pytest.mark.unit
@pytest.mark.django_db
def test_save_phone_number(test_user: User):
    phone = "+79530022680"
    user_repo.save_phone_number(phone=phone, t_id=test_user.id)
    user_phone = user_repo.get_user(t_id=test_user.id).phone
    assert user_phone == phone


@pytest.mark.unit
@pytest.mark.django_db
def test_set_bad_password(test_user: User):
    password = "123456"
    with pytest.raises(InvalidPasswordException):
        user_repo.set_password_for_user(password=password, t_id=test_user.id)


@pytest.mark.unit
@pytest.mark.django_db
def test_set_password(test_user: User):
    password = "Az1asdfg"
    user_repo.set_password_for_user(password=password, t_id=test_user.id)
    password = hash_password(password)
    user_password = user_repo.get_user(t_id=test_user.id).password
    assert user_password == password
