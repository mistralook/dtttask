import json

import pytest
from app.internal.bot import TBot

from app.internal.utils.exceptions import NotFoundException, InvalidPasswordException, InvalidTokenException


@pytest.mark.smoke
def test_bot():
    bot = TBot()
    bot.start()


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_when_all_is_okay(test_user, test_tokens, test_api_client):
    _, access = test_tokens
    auth_headers = {
        "HTTP_AUTHORIZATION": "Bearer " + access,
    }
    resp = test_api_client.get("/api/users/me", **auth_headers)
    assert resp.status_code == 200
    data = json.loads(resp.content.decode("utf-8"))
    assert data["username"] == test_user.username


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_when_invalid_token(test_api_client):
    auth_headers = {
        "HTTP_AUTHORIZATION": "Bearer " + "access",
    }
    resp = test_api_client.get("/api/users/me", **auth_headers)
    assert resp.status_code == 401


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_login_no_user(test_api_client):
    payload = {"username": "test_user.username", "password": "123456Zz"}
    with pytest.raises(NotFoundException):
        test_api_client.post("/api/login", payload, content_type="application/json")


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_login_incorrect_password(test_user, test_api_client):
    payload = {"username": test_user.username, "password": "password"}
    with pytest.raises(InvalidPasswordException):
        test_api_client.post("/api/login", payload, content_type="application/json")


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_login(test_user, test_api_client):
    payload = {"username": test_user.username, "password": "123456Zz"}
    resp = test_api_client.post("/api/login", payload, content_type="application/json")
    assert resp.status_code == 200
    data = json.loads(resp.content.decode("utf-8"))
    assert data["refresh_token"] is not None and data["access_token"] is not None


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_update_incorrect_token(test_api_client):
    payload = {"refresh_token": "refresh"}
    with pytest.raises(NotFoundException):
        test_api_client.post("/api/update_tokens", payload,
                             content_type="application/json")


@pytest.mark.smoke
@pytest.mark.django_db
def test_api_update(test_api_client, test_tokens):
    refresh, _ = test_tokens
    payload = {"refresh_token": refresh}
    resp = test_api_client.post("/api/update_tokens", payload,
                                content_type="application/json")
    assert resp.status_code == 200
    data = json.loads(resp.content.decode("utf-8"))
    assert data["refresh_token"] is not None and data["access_token"] is not None
