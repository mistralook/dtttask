import pytest
from django.test import Client


@pytest.fixture(scope="function")
def test_api_client():
    return Client()
