import pytest
import decimal

from app.internal.users.db.models import User
from app.internal.accounts.db.models import Account
from app.internal.cards.db.models import Card
from app.internal.authentication.db.repositories import IssuedTokenRepository
from app.internal.utils.utils import hash_password

issued_token_repo = IssuedTokenRepository()


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_user(first_name="test_user", username="test_username", id=1, phone="+79532200648"):
    hashed_password = hash_password("123456Zz")
    return User.objects.create(first_name=first_name,
                               last_name="",
                               username=username,
                               password=hashed_password,
                               id=id,
                               phone=phone)


@pytest.fixture(scope="function")
def test_tokens(test_user):
    return issued_token_repo.generate_tokens(test_user)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_account(account_number=123456, account_holder=test_user):
    return Account.objects.create(account_number=account_number, account_holder=account_holder)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_user_with_card_and_account(card_number=int("1" * 16), balance=decimal.Decimal(1000)):
    user = User.objects.create(first_name="test",
                               last_name="",
                               username="test",
                               id=1234,
                               phone="+79530202648")

    account = Account.objects.create(account_number=12345, account_holder=user)

    card = Card.objects.create(card_number=card_number,
                               balance=balance,
                               account=account)
    return user, account, card


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_another_user_with_card_and_account(card_number=int("9" * 16), balance=decimal.Decimal(2000)):
    user = User.objects.create(first_name="test_another",
                               last_name="",
                               username="test_another",
                               id=4321,
                               phone="+79532002648")

    account = Account.objects.create(account_number=54321, account_holder=user)

    card = Card.objects.create(card_number=card_number,
                               balance=balance,
                               account=account)
    return user, account, card


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_third_user_with_card_and_account(card_number=int("8" * 16), balance=decimal.Decimal(2000)):
    user = User.objects.create(first_name="lololoshkin",
                               last_name="",
                               username="lololoshkin",
                               id=228137,
                               phone="+79532022648")

    account = Account.objects.create(account_number=282717, account_holder=user)

    card = Card.objects.create(card_number=card_number,
                               balance=balance,
                               account=account)
    return user, account, card
