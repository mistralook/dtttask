from ninja import NinjaAPI

from .utils.http_jwt_auth import HTTPJwtAuth
from .users.app import get_users_api
from .authentication.app import get_auth_api
from .cards.app import get_cards_api
from .bank_operations.app import get_bank_operations_api
from .accounts.app import get_accounts_api


def get_api() -> NinjaAPI:
    api = NinjaAPI(
        title="backend-course",
        version="1.0.0",
        auth=[HTTPJwtAuth()],
    )

    get_users_api(api)
    get_auth_api(api)
    get_cards_api(api)
    get_bank_operations_api(api)
    get_accounts_api(api)
    return api
