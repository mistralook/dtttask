import decimal

from django.db import transaction
from django.db.models import F
# from app.internal.models.bank_operation import BankOperation
from ...bank_operations.db.models import BankOperation
from ...accounts.db.models import Account
from .models import Card


class ICardRepository:
    def get_balance(self, t_id, card_number):
        ...

    def transfer_money(self, source_card_number: int, destination_card_number: int, amount):
        ...

    def update_card(self, card_number, account) -> Card:
        ...

    def is_card_exists(self, card_number):
        ...


class CardRepository(ICardRepository):
    def get_balance(self, t_id, card_number):
        balance = Card.objects.filter(account__account_holder=t_id, card_number=card_number).values("balance").first()
        account = Account.objects.filter(account_holder=t_id).first()
        if balance is None or account is None:
            return None, None
        return account.account_number, balance['balance']

    def transfer_money(self, from_card, to_card, amount):
        amount = decimal.Decimal(amount)
        with transaction.atomic():
            from_card = Card.objects.select_for_update().filter(card_number=from_card).first()
            if from_card.balance < amount:
                return False
            to_card = Card.objects.select_for_update().filter(card_number=to_card).first()
            from_card.balance = F('balance') - amount
            to_card.balance = F('balance') + amount
            from_card.save(update_fields=('balance',))
            to_card.save(update_fields=('balance',))
            BankOperation.objects.create(source_card=from_card, receiver_card=to_card, amount=amount)
        return True

    def update_card(self, card_number, account) -> Card:
        card = Card.objects.filter(card_number=card_number).first()
        account = Account.objects.filter(account_number=account).first()
        card.account = account
        card.save()
        return card

    def is_card_exists(self, card_number):
        return Card.objects.filter(card_number=card_number).exists()

    @staticmethod
    def get_card_by_card_number(card_number):
        return Card.objects.filter(card_number=card_number).first()
