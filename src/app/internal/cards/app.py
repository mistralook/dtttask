from ninja import NinjaAPI

from .db.repositories import CardRepository
from .domain.services import CardService
from .presentation.handlers import CardHandlers
from .presentation.routers import add_card_router


def get_cards_api(api: NinjaAPI):
    card_repo = CardRepository()
    card_service = CardService(card_repo=card_repo)
    card_handler = CardHandlers(card_service=card_service)
    add_card_router(api, card_handler)
