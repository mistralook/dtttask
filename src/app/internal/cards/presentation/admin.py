from django.contrib import admin

from ..db.models import Card


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass
