from ninja import Body, Path

from ..domain.services import CardService
from ..domain.entities import CardTransferSchemaIn, CardTransferSchemaOut, CardOut, UpdateDataIn


class CardHandlers:
    def __init__(self, card_service: CardService):
        self._card_service = card_service

    def get_card_balance(self, request, t_id: int = Path(...), card_number: int = Path(...)) -> CardOut:
        return self._card_service.get_balance(t_id, card_number)

    def transfer(self, request, transfer_data: CardTransferSchemaIn = Body(...)) -> CardTransferSchemaOut:
        source_card = transfer_data.source_card_number
        destination_card = transfer_data.destination_card_number
        amount = transfer_data.amount
        return self._card_service.transfer_money(source_card, destination_card, amount)

    def update_card(self, request, updated_card_number: int = Path(...),
                    update_data: UpdateDataIn = Body(...)) -> CardOut:
        return self._card_service.update_card(updated_card_number, update_data.account)
