from ninja import NinjaAPI, Router

from ..domain.entities import CardOut, CardTransferSchemaOut
from .handlers import CardHandlers
from ...utils.responses import ErrorResponse


def get_card_router(card_handlers: CardHandlers):
    router = Router(tags=["cards"])
    router.add_api_operation(
        "/balance/source_card:{int:t_id}/{int:card_number}",
        ["GET"],
        card_handlers.get_card_balance,
        response={200: CardOut, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/transfer-money",
        ["POST"],
        card_handlers.transfer,
        response={200: CardTransferSchemaOut, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/update-card/{int:updated_card_number}",
        ["PUT"],
        card_handlers.update_card,
        response={200: CardOut, 400: ErrorResponse},
    )
    return router


def add_card_router(api: NinjaAPI, card_handlers: CardHandlers):
    card_router = get_card_router(card_handlers)
    api.add_router("/cards", card_router)
