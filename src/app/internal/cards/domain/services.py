from ..db.repositories import ICardRepository
from .entities import CardOut, CardTransferSchemaOut
from ...utils.constants import FEATURED_USER_NOT_EXIST, USER_NOT_IN_FAVOURITE_LIST
from ...utils.exceptions import NotFoundException, SelfTransferException, InvalidAmountException


class CardService:
    def __init__(self, card_repo: ICardRepository):
        self._card_repo = card_repo

    def get_balance(self, t_id, card_number) -> CardOut:
        account, balance = self._card_repo.get_balance(t_id, card_number)
        if balance is None:
            raise NotFoundException()
        return CardOut(card_number=card_number, account=account, balance=balance)

    def transfer_money(self, source_card_number, destination_card_number, amount) -> CardTransferSchemaOut:
        if source_card_number == destination_card_number:
            raise SelfTransferException()

        destination_card_number = int(destination_card_number)
        source_card_number = int(source_card_number)

        if not self._card_repo.is_card_exists(destination_card_number):
            raise NotFoundException()

        okay_status = self._card_repo.transfer_money(source_card_number, destination_card_number, amount)
        if not okay_status:
            raise InvalidAmountException()

        return CardTransferSchemaOut(source_card_number=source_card_number,
                                     destination_card_number=destination_card_number,
                                     amount=amount,
                                     message="transfer was successful")

    def update_card(self, card_number, account) -> CardOut:
        if not self._card_repo.is_card_exists(card_number):
            raise NotFoundException()

        card = self._card_repo.update_card(card_number, account)
        return CardOut(card_number=card.card_number, account=card.account.account_number, balance=card.balance)
