from ninja import Schema
from pydantic import Field


class CardTransferSchemaIn(Schema):
    source_card_number: int = Field(gt=15)
    destination_card_number: int = Field(gt=15)
    amount: int


class CardTransferSchemaOut(Schema):
    source_card_number: int = Field(gt=15)
    destination_card_number: int = Field(gt=15)
    amount: int = Field(None)
    message: str = Field(None)


class CardOut(Schema):
    card_number: int = Field(gt=15)
    account: int = Field(None)
    balance: int = Field(None)


class UpdateDataIn(Schema):
    account: int = Field(None)
