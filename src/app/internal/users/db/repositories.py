import re

from .models import User
from ...utils.constants import PASSWORD_TEMPLATE, PASSWORD_SET, COMMAND_BEFORE_START, NO_PHONE, ZERO_FAVOURITES, \
    FEATURED_USER_NOT_EXIST, USER_NOT_IN_FAVOURITE_LIST
from ...utils.exceptions import InvalidPasswordException
from ...utils.utils import hash_password, formatting_favourites


class IUserRepository:
    def get_user(self, t_id):
        ...

    def create_user(self, first_name, last_name, username, t_id):
        ...

    def save_phone_number(self, phone, t_id):
        ...

    def set_password_for_user(self, t_id, password):
        ...

    def check_authorization(self, t_id):
        ...

    def get_user_via_username(self, username):
        ...

    def user_favourites(self, t_id):
        ...

    def add_to_user_favourites(self, t_id, user_to_add):
        ...

    def remove_from_user_favourites(self, t_id, user_to_remove):
        ...


class UserRepository(IUserRepository):
    def get_user(self, t_id):
        user = User.objects.filter(id=t_id).first()
        return user

    def create_user(self, first_name, last_name, username, t_id):
        obj, created = User.objects.get_or_create(
            id=t_id, defaults={"phone": "", "first_name": first_name, "last_name": last_name, "username": username}
        )
        return obj, created

    def save_phone_number(self, phone, t_id):
        obj, updated = User.objects.update_or_create(id=t_id, defaults={"phone": phone})

    def set_password_for_user(self, t_id, password):
        user = self.get_user(t_id)
        if len(password) <= 6 or not password.isalnum() or re.match(r".*(?=.*[0-9]).*(?=.*[A-Z]).*", password) is None:
            raise InvalidPasswordException()
        password = hash_password(password)
        user.password = password
        user.save(update_fields=("password",))
        return PASSWORD_SET

    def get_user_via_username(self, username):
        return User.objects.filter(username=username).first()

    def user_favourites(self, t_id):
        user = self.get_user(t_id)
        return user.favourites.all()

    def add_to_user_favourites(self, t_id, user_to_add):
        user = self.get_user(t_id)
        featured_user = self.get_user_via_username(user_to_add)
        if featured_user is None:
            return False
        user.favourites.add(featured_user)
        user.save()
        return True

    def remove_from_user_favourites(self, t_id, user_to_remove: str):
        user = self.get_user(t_id)
        if not user.favourites.filter(username=user_to_remove).exists():
            return False
        featured_user_to_remove = self.get_user_via_username(user_to_remove)
        user.favourites.remove(featured_user_to_remove)
        user.save()
        return True

    def check_authorization(self, t_id):
        user = self.get_user(t_id)
        if user is None:
            return COMMAND_BEFORE_START, False
        else:
            if user.phone:
                return "", True
            else:
                return NO_PHONE, False
