from ninja import NinjaAPI

from .db.repositories import UserRepository
from .domain.services import UserService
from .presentation.handlers import UserHandlers
from .presentation.routers import add_users_router


def get_users_api(api: NinjaAPI):
    user_repo = UserRepository()
    user_service = UserService(user_repo=user_repo)
    user_handler = UserHandlers(user_service=user_service)
    add_users_router(api, user_handler)
