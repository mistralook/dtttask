from ..db.repositories import IUserRepository
from .entities import UserOut, UserSchema
from ...utils.constants import FEATURED_USER_NOT_EXIST, USER_NOT_IN_FAVOURITE_LIST
from ...utils.exceptions import NotFoundException


class UserService:
    def __init__(self, user_repo: IUserRepository):
        self._user_repo = user_repo

    def create_user(self, user: UserSchema) -> UserOut:
        user, _ = self._user_repo.create_user(user.first_name, user.last_name, user.username, user.id)
        return UserOut.from_orm(user)

    def get_user(self, t_id) -> UserOut:
        user = self._user_repo.get_user(t_id)
        return UserOut.from_orm(user)

    def show_user_favourites(self, t_id) -> [UserOut]:
        if self._user_repo.get_user(t_id) is not None:
            favourites = self._user_repo.user_favourites(t_id)
            return [UserOut.from_orm(favourite) for favourite in favourites]
        else:
            raise NotFoundException()

    def add_to_favourites_list(self, t_id, user_to_add: str) -> str:
        ###TODO: на сервисном слое завернуть ответ в схему, а не в строчках отправлять
        if self._user_repo.get_user(t_id) is not None and self._user_repo.get_user_via_username(user_to_add) is not None:
            status_is_okay = self._user_repo.add_to_user_favourites(t_id, user_to_add)
            if status_is_okay:
                return f"{user_to_add} was successfully added to your favourites list"
            else:
                return FEATURED_USER_NOT_EXIST
        else:
            raise NotFoundException()

    def remove_from_favourites_list(self, t_id, user_to_remove: str) -> str:
        ###TODO: на сервисном слое завернуть ответ в схему, а не в строчках отправлять
        if self._user_repo.get_user(t_id) is not None and self._user_repo.get_user_via_username(user_to_remove) is not None:
            status_is_okay = self._user_repo.remove_from_user_favourites(t_id, user_to_remove)
            if status_is_okay:
                return f"{user_to_remove} was successfully removed from your favourites list"
            else:
                return USER_NOT_IN_FAVOURITE_LIST
        else:
            raise NotFoundException()
