from ninja import Schema
from pydantic import Field


class UserSchema(Schema):
    first_name: str = Field(max_length=64)
    last_name: str = Field(max_length=64)
    username: str = Field(max_length=32)
    id: str = Field()


class UserOut(Schema):
    id: int = Field(None)
    username: str = Field(None)


class UserIn(Schema):
    username: str = Field(None)
    password: str = Field(None)
