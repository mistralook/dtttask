from ninja import NinjaAPI, Router
from .handlers import UserHandlers
from ..domain.entities import UserOut
from ...utils.responses import NotFoundResponse, OKResponse, ErrorResponse


def get_users_router(user_handlers: UserHandlers):
    router = Router(tags=['users'])
    router.add_api_operation(
        '',
        ['POST'],
        user_handlers.create_user,
        response={200: UserOut, 400: ErrorResponse}
    )
    router.add_api_operation(
        '/me',
        ['GET'],
        user_handlers.me,
        response={200: UserOut, 404: NotFoundResponse}
    )
    router.add_api_operation(
        '/show-favourites/{int:t_id}',
        ['GET'],
        user_handlers.show_favourites,
        response={200: list[UserOut], 404: NotFoundResponse}
    )
    router.add_api_operation(
        '/add-to-favourites/{int:t_id}',
        ['POST'],
        user_handlers.add_to_favourites,
        response={200: OKResponse, 404: NotFoundResponse}
    )
    router.add_api_operation(
        '/remove-from-favourites/owner:{int:t_id}/to-remove:{str:user_to_remove}',
        ['DELETE'],
        user_handlers.remove_from_favourites,
        response={200: OKResponse, 404: NotFoundResponse}
    )
    return router


def add_users_router(api: NinjaAPI, user_handlers: UserHandlers):
    users_handler = get_users_router(user_handlers)
    api.add_router('/users', users_handler)
