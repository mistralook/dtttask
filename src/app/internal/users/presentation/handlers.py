from ninja import Path, Body
from ..domain.entities import UserOut, UserSchema
from ..domain.services import UserService
from ...utils.responses import OKResponse


class UserHandlers:
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    def create_user(self, request, user: UserSchema = Body(...)) -> UserOut:
        user = self._user_service.create_user(user)
        return user

    def me(self, request) -> UserOut:
        user = request.user
        user_info = self._user_service.get_user(user.id)
        return user_info

    def show_favourites(self, request, t_id: int = Path(...)) -> [UserOut]:
        return self._user_service.show_user_favourites(t_id)

    def add_to_favourites(self, request, t_id: int = Path(...),
                          user_to_add: str = Body(...)) -> OKResponse:
        ###TODO: Сделать схему с только юзернеймом, чтобы не писать first_name и так далее при добавлении
        message = self._user_service.add_to_favourites_list(t_id, user_to_add)
        return OKResponse(status=f"{message}")

    def remove_from_favourites(self, request, t_id: int = Path(...),
                               user_to_remove: str = Path(...)) -> OKResponse:
        message = self._user_service.remove_from_favourites_list(t_id, user_to_remove)
        return OKResponse(status=f"{message}")
