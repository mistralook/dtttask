import datetime

from config.settings import REFRESH_TOKEN_TTL
from ..db.repositories import IIssuedTokenRepository
from .entities import JwtIn, AuthSchema
from ...users.db.repositories import IUserRepository
from ...utils.exceptions import NotFoundException, InvalidPasswordException, InvalidTokenException
from ...utils.utils import hash_password


class AuthenticationService:
    def __init__(self, issued_token_repo: IIssuedTokenRepository, user_repo: IUserRepository):
        self._issued_token_repo = issued_token_repo
        self._user_repo = user_repo

    def login(self, auth_data: AuthSchema):
        user_info = self._user_repo.get_user_via_username(auth_data.username)
        if user_info is None:
            raise NotFoundException()

        is_password_valid = self.validate_password(auth_data.password, user_info.password)
        if not is_password_valid:
            raise InvalidPasswordException()

        self._issued_token_repo.revoke_tokens(user_info)
        refresh, access = self._issued_token_repo.generate_tokens(user_info)
        return access, refresh

    def refresh(self, refresh_token: JwtIn):
        refresh = refresh_token.refresh_token
        issued_token = self._issued_token_repo.get_token(refresh)

        if issued_token is None:
            raise NotFoundException()
        is_token_valid = self.validate_token(issued_token)
        if not is_token_valid:
            raise InvalidTokenException()

        self._issued_token_repo.revoke_token(issued_token)
        refresh, access = self._issued_token_repo.generate_tokens(issued_token.user)
        return access, refresh

    def validate_password(self, payload_password, user_info_password):
        if user_info_password is None:
            return False
        if not self.match_password(payload_password, user_info_password):
            return False
        return True

    @staticmethod
    def validate_token(issued_token):
        if issued_token.revoked:
            return False
        if (issued_token.created_at + REFRESH_TOKEN_TTL).timestamp() < datetime.datetime.today().timestamp():
            return False
        return True

    @staticmethod
    def match_password(password, user_info_password):
        return hash_password(password) == user_info_password
