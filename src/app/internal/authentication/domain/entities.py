from ninja import Schema
from pydantic import Field


class JwtOut(Schema):
    access_token: str = Field(max_length=1337)
    refresh_token: str = Field(max_length=1337)


class AuthSchema(Schema):
    username: str = Field(None)
    password: str = Field(None)


class JwtIn(Schema):
    refresh_token: str = Field(max_length=1337)
