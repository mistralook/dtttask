import datetime

import jwt

from ...users.db.models import User
from .models import IssuedToken
from config.settings import JWT_SECRET, ACCESS_TOKEN_TTL, REFRESH_TOKEN_TTL

from ...utils.utils import hash_password


class IIssuedTokenRepository:
    def get_token(self, refresh_token) -> IssuedToken:
        ...

    def revoke_tokens(self, user):
        ...

    def revoke_token(self, issued_token):
        ...

    def generate_tokens(self, user_info: User):
        ...


class IssuedTokenRepository(IIssuedTokenRepository):
    def get_token(self, refresh_token) -> IssuedToken:
        return IssuedToken.objects.filter(jti=refresh_token).first()

    def revoke_tokens(self, user):
        user.refresh_token.filter(revoked=False).update(revoked=True)

    def revoke_token(self, issued_token):
        issued_token.revoked = True
        issued_token.save()

    def generate_tokens(self, user_info: User):
        today = datetime.datetime.today()
        refresh_ttl = (today + REFRESH_TOKEN_TTL).timestamp()
        access_ttl = (today + ACCESS_TOKEN_TTL).timestamp()
        access = jwt.encode({"username": user_info.username,
                             "telegram_id": user_info.id,
                             "created_at": f"{today}",
                             "exp": access_ttl}, JWT_SECRET, algorithm="HS256")

        refresh = jwt.encode({"created_at": f"{today}",
                              "exp": refresh_ttl}, JWT_SECRET, algorithm="HS256")
        IssuedToken.objects.create(jti=refresh, user=user_info)
        return refresh, access

    @staticmethod
    def validate_token(issued_token):
        if issued_token.revoked:
            return False, "refresh token was revoked"
        if (issued_token.created_at + REFRESH_TOKEN_TTL).timestamp() < datetime.datetime.today().timestamp():
            return False, "refresh token was expired"
        return True, ""

    def validate_password(self, payload_password, user_info_password):
        if user_info_password is None:
            return False, "the password not set"
        if not self.match_password(payload_password, user_info_password):
            return False, "the password is incorrect"
        return True, ""

    @staticmethod
    def match_password(password, user_info_password):
        return hash_password(password) == user_info_password
