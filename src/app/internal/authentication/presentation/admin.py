from django.contrib import admin

from ..db.models import IssuedToken


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    pass
