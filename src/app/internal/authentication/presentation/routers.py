from ninja import NinjaAPI, Router

from app.internal.utils.responses import ErrorResponse

from ..domain.entities import JwtOut
from .handlers import AuthenticationHandlers


def get_authentication_router(auth_handlers: AuthenticationHandlers):
    router = Router(tags=["authentication"])
    router.add_api_operation(
        "/login",
        ["POST"],
        auth_handlers.login,
        response={200: JwtOut, 404: ErrorResponse},
        auth=None,
    )

    router.add_api_operation(
        "/update_tokens",
        ["POST"],
        auth_handlers.refresh,
        response={200: JwtOut, 404: ErrorResponse},
        auth=None,
    )
    return router


def add_authentication_router(api: NinjaAPI, issued_token_handlers: AuthenticationHandlers):
    issued_token_router = get_authentication_router(issued_token_handlers)
    api.add_router("", issued_token_router)
