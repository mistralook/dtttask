from ninja import Body

from ..domain.services import AuthenticationService
from ..domain.entities import AuthSchema, JwtOut, JwtIn


class AuthenticationHandlers:
    def __init__(self, auth_service: AuthenticationService):
        self._auth_service = auth_service

    def login(self, request, auth_data: AuthSchema = Body(...)) -> JwtOut:
        access, refresh = self._auth_service.login(auth_data)
        return JwtOut(access_token=access, refresh_token=refresh)

    def refresh(self, request, refresh_token: JwtIn = Body(...)) -> JwtOut:
        access, refresh = self._auth_service.refresh(refresh_token)
        return JwtOut(access_token=access, refresh_token=refresh)
