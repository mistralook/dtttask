from ninja import NinjaAPI

from ..users.db.repositories import UserRepository
from .db.repositories import IssuedTokenRepository
from .domain.services import AuthenticationService
from .presentation.handlers import AuthenticationHandlers
from .presentation.routers import add_authentication_router


def get_auth_api(api: NinjaAPI):
    issued_token_repo = IssuedTokenRepository()
    user_repo = UserRepository()
    auth_service = AuthenticationService(issued_token_repo=issued_token_repo, user_repo=user_repo)
    auth_handler = AuthenticationHandlers(auth_service=auth_service)
    add_authentication_router(api, auth_handler)
