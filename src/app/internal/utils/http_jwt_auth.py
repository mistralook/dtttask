import datetime

import jwt
from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.users.db.repositories import UserRepository
from config.settings import JWT_SECRET

user_repo = UserRepository()


class HTTPJwtAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        try:
            payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
            if payload["exp"] < datetime.datetime.today().timestamp():
                return None
            user = user_repo.get_user_via_username(payload["username"])
            request.user = user
        except (KeyError, jwt.InvalidSignatureError, jwt.ExpiredSignatureError, jwt.DecodeError):
            return None
        return token
