import re

from hashlib import pbkdf2_hmac
from config.settings import SALT


def formatting_favourites(favourites):
    favourites = favourites.values("first_name", "username").all()
    format_favourites = [f'{u["first_name"]} (@{u["username"]})' for u in favourites]
    return format_favourites


def hash_password(password):
    password = pbkdf2_hmac('sha512', password.encode("utf-8"), SALT.encode("utf-8"), 1).hex()
    return password


def right_card_format(card_number):
    card_number = card_number.replace("-", "")
    if len(card_number) != 16 or re.match(r"\d{16}", card_number) is None:
        return False
    return card_number
