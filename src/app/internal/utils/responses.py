from ninja import Schema


class NotFoundResponse(Schema):
    status: str


class OKResponse(Schema):
    status: str


class ErrorResponse(Schema):
    status: str
