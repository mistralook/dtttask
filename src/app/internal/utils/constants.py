HELP_MESSAGE = f"/start - register yourself in the bot\n" \
               f"/set_phone - required command to make further commands\n" \
               f"/me - info about you\n" \
               f"/balance_by_card (card_number) - get balance of your card\n" \
               f"/balance_by_account (account_number) - get balance of your account\n" \
               f"/transfer_money (from_card_number) (to_card_number) (amount_of_money) - transfer money\n" \
               f"/show_favourites - show list of your favourites users\n" \
               f"/add_to_favourites (telegram_username) - add to favourites list\n" \
               f"/remove_from_favourites (telegram_username) - remove from favourites list\n" \
               f"/bank_operations_partners - show all telegram users, that you have interacted with\n" \
               f"/bank_operations (card_number) - show all operations of passed card\n" \
               f"/set_password (password) - establishing password for your account"

USER_ALREADY_CREATED = "User was already created!"
NUMBER_WAS_ADDED = "Number was added."
PHONE_FORMAT = 'Please, write the mobile phone in the format: "+7..."'
INVALID_PHONE = "The inputed number is incorrect. :( Repeat again!"
COMMAND_BEFORE_START = "First you need to enter the command /start"
NO_PHONE = "You need to enter this command /set_phone!"
ZERO_FAVOURITES = "You don't have any favourites at the moment."
FEATURED_USER_NOT_EXIST = "Invalid operation. The user to add does not exist"

TEMPLATE_ADD_TO_FAVOURITES = f"To add user to favourites list, use this template:\n " \
                             f"/add_to_favourites (login)\n" \
                             f"F.e. /add_to_favourites Durov"

TEMPLATE_REMOVE_FROM_FAVOURITES = f"To remove user from favourites list, use this template:\n " \
                                  f"/remove_from_favourites (login)\n" \
                                  f"F.e. /remove_from_favourites Durov"

TEMPLATE_CARD_BALANCE = f"To get balance via card number, you need to write it after.\n" \
                        f"F.e. /balance_by_card 1234546789012345"

TEMPLATE_ACCOUNT_BALANCE = f"To get balance via account number, you need to write it after.\n" \
                           f"F.e. /balance_by_account 1234..."

TEMPLATE_OPERATIONS = f"To get all operations of card, you need to write it after.\n" \
                      f"F.e. /bank_operations 1234..."

TEMPLATE_TRANSFER = f"To transfer money, use this template:\n" \
                    f"/transfer_money (from_card_number) (to_card_number|to_telegram_login) (amount_of_money).\n" \
                    f"F.e. /transfer_money 1111-2222-3333-4444 @Durov 1337"

NO_PARTNERS = "Currently you don't have any operation partners."
NO_OPERATIONS = "No operations with this card."
CARD_NOT_FOUND = "Card not found"
RIGHT_CARD_FORMATTING = "Please, write card number in format xxxx-yyyy-zzzz-wwww or xxxxyyyyzzzzwwww"
ACCOUNT_NOT_FOUND = "Account not found"
USER_NOT_IN_FAVOURITE_LIST = "Invalid operation. User is not in your favourites list"
SELF_TRANSFER_ERROR = "Invalid operation. You can't send money to yourself."
INVALID_TRANSFER_ARGUMENTS = "Invalid command. Type '/transfer_money' to see the template."
NOT_ENOUGH_MONEY = "Invalid operation. Not enough money on this card."
SOURCE_CARD_NOT_EXIST = "Invalid source. Card does not exist"
RECEIVER_CARD_NOT_EXIST = "Invalid receiver. Card does not exist"
RECEIVER_USER_NOT_EXIST = "Invalid receiver. User does not exist."
RECEIVER_USER_ZERO_CARDS = "Invalid receiver. User does not have cards."


TEMPLATE_SET_PASSWORD = f"To set a password, you need to write it after.\n" \
                        f"F.e. /set_password 1234567Aa"
PASSWORD_SET = "Password successfully set!"
PASSWORD_TEMPLATE = "Password should be more than 6 characters and contain at least one number and one capital letter"
