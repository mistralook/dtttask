class NotFoundException(Exception):
    pass


class InvalidPasswordException(Exception):
    pass


class InvalidTokenException(Exception):
    pass


class SelfTransferException(Exception):
    pass


class InvalidAmountException(Exception):
    pass