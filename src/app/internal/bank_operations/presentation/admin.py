from django.contrib import admin

from ..db.models import BankOperation


@admin.register(BankOperation)
class BankOperationAdmin(admin.ModelAdmin):
    pass
