from ninja import NinjaAPI, Router

from ..domain.entities import BankOperationPartnerOut, BankOperationOut
from .handlers import BankOperationHandlers
from ...utils.responses import ErrorResponse


def get_bank_operation_router(bank_operation_handler: BankOperationHandlers):
    router = Router(tags=["bank_operations"])
    router.add_api_operation(
        "/user_id:{int:t_id}/bank_operations_partners",
        ["GET"],
        bank_operation_handler.bank_operations_partners,
        response={200: list[BankOperationPartnerOut], 400: ErrorResponse},
    )

    router.add_api_operation(
        "/user_id:{int:t_id}/card_number:{int:card_number}/show_bank_operations",
        ["GET"],
        bank_operation_handler.show_bank_operations,
        response={200: list[BankOperationOut], 400: ErrorResponse},
    )

    return router


def add_bank_operation_router(api: NinjaAPI, bank_operation_handler: BankOperationHandlers):
    bank_operation_router = get_bank_operation_router(bank_operation_handler)
    api.add_router("/bank_operations", bank_operation_router)
