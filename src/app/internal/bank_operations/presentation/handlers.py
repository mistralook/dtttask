from ninja import Body, Path

from ..domain.services import BankOperationService
from ..domain.entities import BankOperationPartnerOut, BankOperationOut


class BankOperationHandlers:
    def __init__(self, bank_operation_service: BankOperationService):
        self._bank_operation_service = bank_operation_service

    def bank_operations_partners(self, request, t_id: int = Path(...)) -> list[BankOperationPartnerOut]:
        return self._bank_operation_service.operations_partners(t_id)

    def show_bank_operations(self, request, t_id: int = Path(...),
                             card_number: int = Path(...)) -> list[BankOperationOut]:
        return self._bank_operation_service.get_all_operations(t_id, card_number)
