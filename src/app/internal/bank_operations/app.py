from ninja import NinjaAPI

from .db.repositories import BankOperationRepository
from .domain.services import BankOperationService
from .presentation.handlers import BankOperationHandlers
from .presentation.routers import add_bank_operation_router


def get_bank_operations_api(api: NinjaAPI):
    bank_operation_repo = BankOperationRepository()
    bank_operation_service = BankOperationService(bank_operation_repo=bank_operation_repo)
    bank_operation_handler = BankOperationHandlers(bank_operation_service=bank_operation_service)
    add_bank_operation_router(api, bank_operation_handler)
