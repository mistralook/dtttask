from ninja import Schema
from pydantic import Field


class BankOperationPartnerOut(Schema):
    username: str = Field(max_length=228)


class BankOperationOut(Schema):
    from_card: int = Field(None)
    to_card: int = Field(None)
    amount: int = Field(None)
