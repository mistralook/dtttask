from ..db.repositories import IBankOperationRepository
from .entities import BankOperationPartnerOut, BankOperationOut
from ...utils.exceptions import NotFoundException


class BankOperationService:
    def __init__(self, bank_operation_repo: IBankOperationRepository):
        self._bank_operation_repo = bank_operation_repo

    def operations_partners(self, t_id) -> list[BankOperationPartnerOut]:
        partners = self._bank_operation_repo.operations_partners(t_id)
        if not partners:
            raise NotFoundException()
        return [BankOperationPartnerOut(username=partner[0]) for partner in partners]

    def get_all_operations(self, t_id, card_number) -> list[BankOperationOut]:
        operations = self._bank_operation_repo.get_all_operations(t_id, card_number)
        if not operations:
            raise NotFoundException()
        return [BankOperationOut(from_card=operation["source_card"],
                                 to_card=operation["receiver_card"],
                                 amount=operation["amount"]) for operation in operations]
