from django.db import models

from ...cards.db.models import Card


class BankOperation(models.Model):
    source_card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="sender_card")
    receiver_card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="recipient_card")
    amount = models.DecimalField(max_digits=19, decimal_places=2, null=True)

    def __str__(self):
        return f"Source: {self.source_card}, receiver: {self.receiver_card}, amount: {self.amount}"
