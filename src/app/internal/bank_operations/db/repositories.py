from .models import BankOperation
from ...users.db.repositories import UserRepository
from ...cards.db.repositories import CardRepository
from django.db.models import Q


class IBankOperationRepository:
    def operations_partners(self, t_id):
        ...

    def get_all_operations(self, t_id, card_number):
        ...


class BankOperationRepository(IBankOperationRepository):
    def operations_partners(self, t_id):
        user_repo = UserRepository()
        user = user_repo.get_user(t_id=t_id)
        operations_from = BankOperation.objects.filter(source_card__account__account_holder=user) \
            .values_list("receiver_card__account__account_holder__username")
        operations_to = BankOperation.objects.filter(receiver_card__account__account_holder=user) \
            .values_list("source_card__account__account_holder__username")
        usernames = operations_from.union(operations_to)
        if not usernames:
            return False
        # return f", ".join([f'@{u[0]}' for u in usernames])
        return usernames

    def get_all_operations(self, t_id, card_number):
        card_repo = CardRepository()
        card = card_repo.get_card_by_card_number(card_number)

        if not card or card.account.account_holder_id != t_id:
            return False

        operations = BankOperation.objects.filter(Q(source_card=card) | Q(receiver_card=card)) \
            .values("source_card__account__account_holder__username",
                    "receiver_card__account__account_holder__username",
                    "source_card", "receiver_card", "amount").all()

        if not operations:
            return False

        formatted = [
            f'From: {operation["source_card"]}(@{operation["source_card__account__account_holder__username"]}) \n' \
            f'To: {operation["receiver_card"]}(@{operation["receiver_card__account__account_holder__username"]}) \n' \
            f'Amount: {operation["amount"]} \n'
            for operation in operations]

        # return "History of transactions: \n" + "".join(formatted)
        return operations
