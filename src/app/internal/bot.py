# from django.conf import settings
from telegram.ext import CommandHandler, ConversationHandler, MessageHandler, Updater, Filters

from app.internal.bot_handlers.bank_handlers import balance_by_account, balance_by_card, transfer_money, \
    bank_operations_partners, bank_operations
from app.internal.bot_handlers.handlers import error, me, phone_num_handler, set_phone, start_command, show_favourites, \
    add_to_favourites, remove_from_favourites, help_command, set_password
from config.settings import API_TOKEN


class TBot:
    def __init__(self):
        self.updater = Updater(API_TOKEN, use_context=True)
        self.dp = self.updater.dispatcher
        self.start()

    def start(self):
        self.dp.add_handler(CommandHandler("start", start_command))
        conversation_handler = ConversationHandler(
            entry_points=[CommandHandler("set_phone", set_phone)],
            states={1: [MessageHandler(Filters.text, phone_num_handler, pass_user_data=True)]},
            fallbacks=[],
        )
        self.dp.add_handler(conversation_handler)
        self.dp.add_handler(CommandHandler("me", me))
        self.dp.add_handler(CommandHandler("help", help_command))
        self.dp.add_handler(CommandHandler("balance_by_card", balance_by_card))
        self.dp.add_handler(CommandHandler("balance_by_account", balance_by_account))
        self.dp.add_handler(CommandHandler("transfer_money", transfer_money))
        self.dp.add_handler(CommandHandler("show_favourites", show_favourites))
        self.dp.add_handler(CommandHandler("add_to_favourites", add_to_favourites))
        self.dp.add_handler(CommandHandler("remove_from_favourites", remove_from_favourites))
        self.dp.add_handler(CommandHandler("bank_operations_partners", bank_operations_partners))
        self.dp.add_handler(CommandHandler("bank_operations", bank_operations))
        self.dp.add_handler(CommandHandler("set_password", set_password))
        self.dp.add_error_handler(error)

    def poll(self):
        self.updater.start_polling()
        self.updater.idle()
        # self.updater.start_webhook(listen="0.0.0.0",
        #                            port=8443,
        #                            url_path=API_TOKEN,
        #                            key="/etc/letsencrypt/live/triplehover.backend22.2tapp.cc/privkey.pem",
        #                            cert="/etc/letsencrypt/live/triplehover.backend22.2tapp.cc/fullchain.pem",
        #                            webhook_url="https://triplehover.backend22.2tapp.cc:8443/" + API_TOKEN)
        # self.updater.idle()
