import re

from telegram.ext import ConversationHandler
from ..utils.constants import HELP_MESSAGE, USER_ALREADY_CREATED, PHONE_FORMAT, \
    INVALID_PHONE, NUMBER_WAS_ADDED, TEMPLATE_ADD_TO_FAVOURITES, TEMPLATE_REMOVE_FROM_FAVOURITES, \
    TEMPLATE_SET_PASSWORD, PASSWORD_TEMPLATE, ZERO_FAVOURITES, FEATURED_USER_NOT_EXIST, USER_NOT_IN_FAVOURITE_LIST
from ..users.db.repositories import UserRepository
from ..utils.exceptions import InvalidPasswordException

user_repo = UserRepository()


def help_command(update, context):
    help_msg = HELP_MESSAGE
    update.message.reply_text(help_msg)


def start_command(update, context):
    user_info = update.message.from_user
    t_id = user_info.id
    first_name = user_info.first_name
    last_name = user_info.last_name
    username = user_info.username
    _, created = user_repo.create_user(first_name, last_name, username, t_id)
    if created:
        update.message.reply_text(f"Пользователь был сохранен, {first_name}!")
    else:
        update.message.reply_text(USER_ALREADY_CREATED)


def set_phone(update, context):
    update.message.reply_text(PHONE_FORMAT)
    return 1


def phone_num_handler(update, context):
    phone_number = str(update.message.text)
    if len(phone_number) != 12 or re.match(r"^(\+7+([0-9]){10})", phone_number) is None:
        update.message.reply_text(INVALID_PHONE)
        return ConversationHandler.END
    else:
        t_id = update.message.from_user.id
        user_repo.save_phone_number(phone_number, t_id)
        update.message.reply_text(NUMBER_WAS_ADDED)
        return ConversationHandler.END


def set_password(update, context):
    t_id = update.message.from_user.id
    args = context.args
    reason, authorized = user_repo.check_authorization(t_id)
    if not args or len(args) > 1:
        return update.message.reply_text(TEMPLATE_SET_PASSWORD)
    if authorized:
        password = args[0]
        try:
            status = user_repo.set_password_for_user(t_id, password)
        except InvalidPasswordException:
            status = PASSWORD_TEMPLATE
        update.message.reply_text(status)
    else:
        update.message.reply_text(reason)


def me(update, context):
    t_id = update.message.from_user.id
    reason, authorized = user_repo.check_authorization(t_id)
    if authorized:
        user_info = user_repo.get_user(t_id)
        full_name = f'{user_info.first_name} {user_info.last_name if user_info.last_name is not None else ""}'
        message = f"Информация о текущем пользователе\n" \
                  f"Имя: {full_name}\n" \
                  f"Имя пользователя: {user_info.username}\n" \
                  f"Номер телефона: {user_info.phone}"
        update.message.reply_text(message)
    else:
        update.message.reply_text(reason)


def show_favourites(update, context):
    t_id = update.message.from_user.id
    reason, authorized = user_repo.check_authorization(t_id)
    if authorized:
        favourites = user_repo.user_favourites(t_id)
        if not favourites:
            return update.message.reply_text(ZERO_FAVOURITES)
        format_favourites = ', '.join([f'{u.first_name} (@{u.username})' for u in favourites])
        answer = f"Your favourites:\n" \
                 f"{format_favourites}"
        update.message.reply_text(answer)
    else:
        update.message.reply_text(reason)


def add_to_favourites(update, context):
    t_id = update.message.from_user.id
    args = context.args
    reason, authorized = user_repo.check_authorization(t_id)
    if not args:
        return update.message.reply_text(TEMPLATE_ADD_TO_FAVOURITES)
    if authorized:
        user_to_add = args[0]
        answer = user_repo.add_to_user_favourites(t_id, user_to_add)
        if not answer:
            return update.message.reply_text(FEATURED_USER_NOT_EXIST)
        update.message.reply_text(f"{user_to_add} was successfully added to your favourites list")
    else:
        update.message.reply_text(reason)


def remove_from_favourites(update, context):
    t_id = update.message.from_user.id
    args = context.args
    reason, authorized = user_repo.check_authorization(t_id)
    if not args:
        return update.message.reply_text(TEMPLATE_REMOVE_FROM_FAVOURITES)

    if authorized:
        user_to_remove = args[0]
        answer = user_repo.remove_from_user_favourites(t_id, user_to_remove)
        if not answer:
            return update.message.reply_text(USER_NOT_IN_FAVOURITE_LIST)
        update.message.reply_text(f"{user_to_remove} was successfully removed from your favourites list")
    else:
        update.message.reply_text(reason)


def error(update, context):
    print(f"Oopsie! Something is wrong: {context.error}")
