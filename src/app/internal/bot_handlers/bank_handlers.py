from ..utils.constants import CARD_NOT_FOUND, ACCOUNT_NOT_FOUND, RIGHT_CARD_FORMATTING, \
    TEMPLATE_CARD_BALANCE, TEMPLATE_ACCOUNT_BALANCE, TEMPLATE_TRANSFER, SELF_TRANSFER_ERROR, INVALID_TRANSFER_ARGUMENTS, \
    TEMPLATE_OPERATIONS, SOURCE_CARD_NOT_EXIST, RECEIVER_CARD_NOT_EXIST, NOT_ENOUGH_MONEY, NO_PARTNERS, NO_OPERATIONS

from ..utils.utils import right_card_format
from ..bank_operations.db.repositories import BankOperationRepository
from ..accounts.db.repositories import AccountRepository
from ..cards.db.repositories import CardRepository

bank_operation_repo = BankOperationRepository()
account_repo = AccountRepository()
card_repo = CardRepository()


def get_balance(t_id, subject, subject_number, balance_method):
    _, balance = balance_method(t_id, subject_number)
    if balance is None:
        if subject == "card":
            return CARD_NOT_FOUND
        elif subject == "account":
            return ACCOUNT_NOT_FOUND
    return f"{balance}"


def check_balance_card(t_id, card_number):
    card_number = right_card_format(card_number)
    if not card_number:
        return RIGHT_CARD_FORMATTING
    balance = get_balance(t_id, "card", card_number, card_repo.get_balance)
    return balance


def validate_amount(amount):
    try:
        amount = int(amount)
        if amount < 0:
            return "amount must be a positive number."
        elif amount == 0:
            return "amount must be bigger than zero"
    except ValueError:
        return "amount must be a positive number."
    return amount


def balance_by_card(update, context):
    t_id = update.message.from_user.id
    args = context.args
    if not args:
        return update.message.reply_text(TEMPLATE_CARD_BALANCE)
    if len(args) == 1:
        card_number = args[0]
        card_number = card_number.replace("-", "")
        answer = check_balance_card(t_id, card_number)
        update.message.reply_text(answer)

    elif len(args) == 4:
        card_number = "".join(nums for nums in args)
        answer = check_balance_card(t_id, card_number)
        update.message.reply_text(answer)


def balance_by_account(update, context):
    t_id = update.message.from_user.id
    args = context.args
    if not args:
        return update.message.reply_text(TEMPLATE_ACCOUNT_BALANCE)
    account_number = args[0]
    balance = get_balance(t_id, "account", account_number, account_repo.get_balance_by_account)
    update.message.reply_text(balance)


def transfer_money(update, context):
    t_id = update.message.from_user.id
    args = context.args
    if not args:
        return update.message.reply_text(TEMPLATE_TRANSFER)
    if len(args) == 3:
        sender_card = right_card_format(args[0].replace("-", ""))
        if not sender_card:
            return update.message.reply_text(RIGHT_CARD_FORMATTING)
        destination = args[1].replace("-", "")

        amount = validate_amount(args[2])
        if type(amount) is str:
            return update.message.reply_text(f"Invalid amount. Reason: {amount}")

        if sender_card == destination:
            return update.message.reply_text(SELF_TRANSFER_ERROR)

        source_card_number = int(sender_card)
        destination_card_number = int(destination)

        if not card_repo.is_card_exists(source_card_number):
            return update.message.reply_text(SOURCE_CARD_NOT_EXIST)

        if not card_repo.is_card_exists(destination_card_number):
            return update.message.reply_text(RECEIVER_CARD_NOT_EXIST)

        status = card_repo.transfer_money(source_card_number, destination_card_number, amount)
        if not status:
            return update.message.reply_text(NOT_ENOUGH_MONEY)

        message = f"The operation was successful. \n" \
                  f"From: {source_card_number} \n" \
                  f"To: {destination_card_number} \n" \
                  f"{amount} were sent"

        update.message.reply_text(message)
    else:
        return update.message.reply_text(INVALID_TRANSFER_ARGUMENTS)


###TODO: перенести комманды операций на репозиторий, удалить сервисы, перенести все, связанное с ботом в отдельную папку
def bank_operations_partners(update, context):
    t_id = update.message.from_user.id
    usernames = bank_operation_repo.operations_partners(t_id)
    if not usernames:
        return update.message.reply_text(NO_PARTNERS)
    partners = f", ".join([f'@{u[0]}' for u in usernames])
    return update.message.reply_text(partners)


def bank_operations(update, context):
    t_id = update.message.from_user.id
    args = context.args
    if not args or len(args) != 1:
        return update.message.reply_text(TEMPLATE_OPERATIONS)
    card_number = args[0]
    card_number = right_card_format(card_number)
    if not card_number:
        return update.message.reply_text(RIGHT_CARD_FORMATTING)
    operations = bank_operation_repo.get_all_operations(t_id, card_number)
    if not operations:
        update.message.reply_text(NO_OPERATIONS)
    formatted = [
        f'From: {operation["source_card"]}(@{operation["source_card__account__account_holder__username"]}) \n' \
        f'To: {operation["receiver_card"]}(@{operation["receiver_card__account__account_holder__username"]}) \n' \
        f'Amount: {operation["amount"]} \n'
        for operation in operations]
    message = "History of transactions: \n" + "".join(formatted)
    return update.message.reply_text(message)
