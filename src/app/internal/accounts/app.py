from ninja import NinjaAPI

from .db.repositories import AccountRepository
from .domain.services import AccountService
from .presentation.handlers import AccountHandlers
from .presentation.routers import add_account_router


def get_accounts_api(api: NinjaAPI):
    account_repo = AccountRepository()
    account_service = AccountService(account_repo=account_repo)
    account_handler = AccountHandlers(account_service=account_service)
    add_account_router(api, account_handler)
