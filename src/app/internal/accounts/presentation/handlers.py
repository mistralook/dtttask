from ninja import Path

from ..domain.services import AccountService
from ..domain.entities import AccountOut


class AccountHandlers:
    def __init__(self, account_service: AccountService):
        self._account_service = account_service

    def get_balance_by_account(self, request, t_id: int = Path(...),
                               account_number: int = Path(...)) -> AccountOut:
        return self._account_service.get_balance_by_account(t_id, account_number)
