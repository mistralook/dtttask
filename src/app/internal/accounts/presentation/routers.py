from ninja import NinjaAPI, Router

from ..domain.entities import AccountOut
from .handlers import AccountHandlers
from ...utils.responses import ErrorResponse


def get_account_router(account_handler: AccountHandlers):
    router = Router(tags=["account"])
    router.add_api_operation(
        "/user_id:{int:t_id}/balance_by_account/account_number:{int:account_number}",
        ["GET"],
        account_handler.get_balance_by_account,
        response={200: AccountOut, 400: ErrorResponse},
    )
    return router


def add_account_router(api: NinjaAPI, account_handler: AccountHandlers):
    account_router = get_account_router(account_handler)
    api.add_router("/account", account_router)
