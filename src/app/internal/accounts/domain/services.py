from ..db.repositories import IAccountRepository
from .entities import AccountOut
from ...utils.exceptions import NotFoundException


class AccountService:
    def __init__(self, account_repo: IAccountRepository):
        self._account_repo = account_repo

    def get_balance_by_account(self, t_id, account_number) -> AccountOut:
        _, balance = self._account_repo.get_balance_by_account(t_id, account_number)
        if not balance:
            raise NotFoundException()
        return AccountOut(account=account_number, balance=balance)
