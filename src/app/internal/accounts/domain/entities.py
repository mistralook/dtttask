from ninja import Schema
from pydantic import Field


class AccountOut(Schema):
    account: int = Field(ge=0)
    balance: int = Field(None)
