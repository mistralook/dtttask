from ...cards.db.models import Card


class IAccountRepository:
    def get_balance_by_account(self, t_id, account_number):
        ...


class AccountRepository(IAccountRepository):
    def get_balance_by_account(self, t_id, account_number):
        balances = Card.objects.filter(account__account_holder=t_id, account=account_number).values("balance").all()
        if not balances:
            return None
        balance = sum([balance['balance'] for balance in balances])
        return account_number, balance
