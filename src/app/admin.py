from django.contrib import admin

# from app.internal.admin.account import AccountAdmin
# from app.internal.admin.admin_user import AdminUserAdmin
# from app.internal.admin.card import CardAdmin
# from app.internal.admin.user import UserAdmin
# from app.internal.admin.bank_operation import BankOperationAdmin
# from app.internal.admin.issued_token import IssuedTokenAdmin

from app.internal.admins.presentation.admin import AdminUserAdmin
from app.internal.accounts.presentation.admin import AccountAdmin
from app.internal.bank_operations.presentation.admin import BankOperationAdmin
from app.internal.cards.presentation.admin import CardAdmin
from app.internal.users.presentation.admin import UserAdmin
from app.internal.authentication.presentation.admin import IssuedTokenAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
